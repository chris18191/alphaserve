import setuptools

long_description = ""
requirements = ""

with open("README.md", "r") as f:
    long_description = f.read()

with open("requirements.txt", "r") as f:
    requirements = f.read().split("\n")

setuptools.setup(
    name="alphaserve",
     version='0.1.2',
     scripts=['alphaserve'] ,
     author="chris18191",
     author_email="chris18191@gmail.com",
     description="A local web server providing an accessible remote for controling your computer",
     long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://bitbucket.org/chris18191/alphaserve/",
    packages=setuptools.find_packages(),
    py_modules=["utils", "control"],
    install_requires=requirements,
     classifiers=[
         "Programming Language :: Python :: 3",
         "License :: OSI Approved :: MIT License",
         "Operating System :: Microsoft :: Windows",
         "Operating System :: Unix",
     ],
 )

