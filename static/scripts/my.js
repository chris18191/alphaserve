function sendCommand(command, param) {
  socket.emit("key_event", {"cmd": command, "param": param});
}

// listens to trackpad events
var trackpad = document.getElementById('trackpad');
var trackManager = new Hammer(trackpad);
trackManager.on("panleft panright panup pandown touchmove mousemove", function(ev) {
  if(Math.abs(ev.velocityX)>= 0.01 || Math.abs(ev.velocityY)>=0.01){
    socket.emit("mouse_event", {MouseEvent: ev.type, deltaX: ev.deltaX, deltaY: ev.deltaY, velocityY: ev.velocityY, velocityX: ev.velocityX, isFinal: ev.isFinal});
  }
});
trackManager.on("tap press", function(ev) {
  socket.emit("mouse_event", {MouseEvent: ev.type});
});

// listen to scrollbar events
var scrollbar = document.getElementById("scrollbar");
var scrollManager = new Hammer(scrollbar);
scrollManager.on("panup pandown", function(ev) {
    console.log({MouseEvent: ev.type, deltaY: ev.deltaY, velocityY: ev.velocityY, isFinal: ev.isFinal});
    socket.emit("mouse_event", {MouseEvent: "scroll", deltaY: ev.deltaY/2, velocityY: ev.velocityY, isFinal: ev.isFinal});
});


