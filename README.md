# **Project AlphaServe**
_currently running on python3 and Flask_
***

## Idea
create a webserver, that receives control commands from clients using:
- *Flask* is used for running the webserver
- *Hammer.js* serves implementations for mouse and click events
- *socket.io* lets us establish an TCP connection, so that the commands are sent faster

## TODOS
- fix CSS for remote to properly render and disable scrolling when using the touchpad
- fix the scrolling bar
- add argument option for en-/disabling the debug mode

